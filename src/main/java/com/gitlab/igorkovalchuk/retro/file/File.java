package com.gitlab.igorkovalchuk.retro.file;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Table("files")
public class File {

  @Id
  private Integer id;

  @Column("owner_id")
  private Integer ownerId;

  private String name;

  private String content;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getOwnerId() {
    return this.ownerId;
  }

  public void setOwnerId(Integer ownerId) {
    this.ownerId = ownerId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  @Override
  public String toString() {
    return "File{" + "id=" + id + ", ownerId='" + ownerId + '\'' + ", name='" + name + '\'' + '}';
  }

}
