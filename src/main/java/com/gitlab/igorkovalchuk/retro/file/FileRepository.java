package com.gitlab.igorkovalchuk.retro.file;

import org.springframework.data.repository.Repository;

public interface FileRepository extends Repository<File, Integer> {

  Iterable<File> findAll();

  Iterable<File> findByOwnerId(Integer ownerId);

  void save(File file);
}
