package com.gitlab.igorkovalchuk.retro.file;

import com.gitlab.igorkovalchuk.retro.owner.Owner;

/**
 * Data Transfer Object for View
 */
public class FileDetails {

  public final File file;
  public final Owner owner;

  public FileDetails(File file, Owner owner) {
    this.file = file;
    this.owner = owner;
  }
}
