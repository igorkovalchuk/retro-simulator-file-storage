package com.gitlab.igorkovalchuk.retro.file;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gitlab.igorkovalchuk.retro.owner.Owner;
import com.gitlab.igorkovalchuk.retro.owner.OwnerRepository;
import static com.gitlab.igorkovalchuk.retro.Util.*;

@Controller
@RequestMapping("/files")
public class FileController {

  private final FileRepository files;
  private final OwnerRepository owners;

  public FileController(FileRepository files, OwnerRepository owners) {
    this.files = files;
    this.owners = owners;
  }

  /**
   * Get the list of all files, URL: /files
   */
  @RequestMapping(method = RequestMethod.GET)
  public String getFiles(Model model) {

    // TODO: probably for JDBC it would be better to use SQL JOIN

    List<Owner> ownersList = toList(owners.findAll());

    List<FileDetails> result = toStream(files.findAll())
        .map(f -> new FileDetails(f, searchOwnerById(ownersList, f.getOwnerId())))
        .collect(Collectors.toList());

    model.addAttribute("files", result);
    return "files";
  }

  private Owner searchOwnerById(List<Owner> list, Integer ownerId) {
    return list.stream()
        .filter(o -> o.getId().equals(ownerId))
        .findFirst()
        .orElse(null);
  }

}
