package com.gitlab.igorkovalchuk.retro;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Util {

  public static <T> List<T> toList(Iterable<T> iterable) {
    List<T> result = new ArrayList<T>();
    Iterator<T> iterator = iterable.iterator();
    while (iterator.hasNext()) {
      result.add(iterator.next());
    }
    return result;
  }

  public static <T> Stream<T> toStream(Iterable<T> iterable) {
    return StreamSupport.stream(iterable.spliterator(), false);
  }

}
