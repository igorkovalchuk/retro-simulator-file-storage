package com.gitlab.igorkovalchuk.retro.owner;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("owners")
public class Owner {

  @Id
  private Integer id;

  private String email;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Override
  public String toString() {
    return "Owner{" + "id=" + id + ", email='" + email + '\'' + '}';
  }

}
