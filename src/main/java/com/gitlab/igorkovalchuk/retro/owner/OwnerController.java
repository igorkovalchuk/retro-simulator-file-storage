package com.gitlab.igorkovalchuk.retro.owner;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gitlab.igorkovalchuk.retro.file.FileDetails;
import com.gitlab.igorkovalchuk.retro.file.FileRepository;
import static com.gitlab.igorkovalchuk.retro.Util.*;

@Controller
@RequestMapping("/owners")
class OwnerController {

  private final OwnerRepository owners;
  private final FileRepository files;

  public OwnerController(OwnerRepository owners, FileRepository files) {
    this.owners = owners;
    this.files = files;
  }

  /**
   * Get the list of all owners, URL: /owners
   */
  @RequestMapping(method = RequestMethod.GET)
  public String get(Model model) {
    model.addAttribute("owners", owners.findAll());
    return "owners";
  }

  /**
   * Search files by owner's email, URL: /owners/files?email=example@example.com
   */
  @RequestMapping(value = "/files", method = RequestMethod.GET)
  public String getFiles(@RequestParam String email, Model model) {

    // TODO: probably for JDBC it would be better to use SQL JOIN

    Optional<Owner> owner = this.owners.findByEmail(email);
    
    List<FileDetails> result = owner.stream()
        .flatMap(o -> toStream(this.files.findByOwnerId(o.getId())))
        .map(f -> new FileDetails(f, owner.get()))
        .collect(Collectors.toList());

    model.addAttribute("files", result);

    return "files";
  }

}
