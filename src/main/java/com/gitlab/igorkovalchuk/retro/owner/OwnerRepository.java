package com.gitlab.igorkovalchuk.retro.owner;

import java.util.Optional;

import org.springframework.data.repository.Repository;

public interface OwnerRepository extends Repository<Owner, Integer> {

  Iterable<Owner> findAll();

  Optional<Owner> findByEmail(String email);

  void save(Owner owner);
}
