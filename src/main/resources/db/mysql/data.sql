INSERT IGNORE INTO owners (id, email) VALUES (1, 'example1@example.com');
INSERT IGNORE INTO owners (id, email) VALUES (2, 'example2@example.com');

INSERT IGNORE INTO files (id, owner_id, name, content) VALUES (1, 1, 'file1 of owner1', 'content1');
INSERT IGNORE INTO files (id, owner_id, name, content) VALUES (2, 1, 'file2 of owner1', 'content2');
INSERT IGNORE INTO files (id, owner_id, name, content) VALUES (3, 2, 'file3 of owner2', 'content3');
INSERT IGNORE INTO files (id, owner_id, name, content) VALUES (4, 2, 'file4 of owner2', 'content4');
